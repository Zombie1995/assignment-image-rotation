#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel { uint8_t r, g, b; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image* create_image(uint32_t width, uint32_t height, struct pixel* data);

void free_image(struct image* img);

#endif
