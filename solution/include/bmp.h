#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

/*  deserializer   */
enum read_status
{
    READ_OK = 0,
    READ_ERROR
    /* коды других ошибок  */
};
enum read_status from_bmp(FILE *in, struct image **img);

/*  serializer   */
enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};
enum write_status to_bmp(FILE *out, struct image const *img);

#endif
