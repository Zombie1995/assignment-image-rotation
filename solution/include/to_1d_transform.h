#ifndef TO_1D_TRANSFORM_H
#define TO_1D_TRANSFORM_H

#include <stdint.h>

uint32_t index1D(uint32_t x, uint32_t y, uint32_t width);

#endif
