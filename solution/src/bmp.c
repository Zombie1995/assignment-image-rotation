#include "bmp.h"
#include "image.h"
#include "to_1d_transform.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t get_padding(uint32_t width) {
    return (4 - width * 3 % 4) % 4;
}

static uint32_t get_image_size(
    uint32_t width, 
    uint32_t height) {
    uint32_t padding = get_padding(width);
    return (3 * width + padding) * height;
}

static void pixel_data_to_pixels(
    struct pixel *pixels, 
    uint8_t const *pixel_data, 
    uint32_t width, 
    uint32_t height) {
    uint32_t padding = get_padding(width);
    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            uint32_t pixel_index = index1D(j, i, width); 
            pixels[pixel_index].r = pixel_data[3 * pixel_index + 0 + i * padding];
            pixels[pixel_index].g = pixel_data[3 * pixel_index + 1 + i * padding];
            pixels[pixel_index].b = pixel_data[3 * pixel_index + 2 + i * padding];
        }
    }
}

static void pixels_to_pixel_data(
    struct pixel const *pixels, 
    uint8_t *pixel_data, 
    uint32_t width, 
    uint32_t height) {
    uint32_t padding = get_padding(width);
    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            uint32_t pixel_index = index1D(j, i, width); 
            pixel_data[3 * pixel_index + 0 + i * padding] = pixels[pixel_index].r;
            pixel_data[3 * pixel_index + 1 + i * padding] = pixels[pixel_index].g;
            pixel_data[3 * pixel_index + 2 + i * padding] = pixels[pixel_index].b;
        }
    }
}

static struct bmp_header new_bmp_header(
    uint32_t biSizeImage, 
    uint32_t width, 
    uint32_t height) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + biSizeImage;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = biSizeImage;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

const size_t bmp_header_size = sizeof(struct bmp_header);
const size_t pixel_size = sizeof(struct pixel);

enum read_status from_bmp(FILE *in, struct image **img) {
    struct bmp_header header;
    
    if (fread(&header, bmp_header_size, 1, in) != 1) {
        return READ_ERROR;
    }
    
    uint32_t biSizeImage = get_image_size(
        header.biWidth, 
        header.biHeight);
    struct pixel *pixels = malloc(
        pixel_size * 
        header.biWidth * 
        header.biHeight);
    uint8_t *pixel_data = malloc(biSizeImage);
    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        free(pixels);
        free(pixel_data);
        return READ_ERROR;
    }
    // fread(pixel_data, header.biSizeImage, 1, in); // Sometimes header.biSizeImage = 0
    if (fread(pixel_data, biSizeImage, 1, in) != 1) {
        free(pixels);
        free(pixel_data);
        return READ_ERROR;
    }

    pixel_data_to_pixels(
        pixels, 
        pixel_data, 
        header.biWidth, 
        header.biHeight);

    *img = create_image(
        header.biWidth,
        header.biHeight,
        pixels);

    free(pixel_data);

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    uint32_t biSizeImage = get_image_size(
        img->width, 
        img->height);
    struct bmp_header header = new_bmp_header(
        biSizeImage, 
        img->width, 
        img->height);
    if (fwrite(&header, bmp_header_size, 1, out) != 1) {
        return WRITE_ERROR;
    }

    uint8_t *pixel_data = malloc(biSizeImage);
    for (uint32_t i = 0; i < biSizeImage; i++) pixel_data[i] = 0; // SANITIZER=msan fix
    pixels_to_pixel_data(
        img->data, 
        pixel_data, 
        img->width, 
        img->height);
    if (fwrite(pixel_data, biSizeImage, 1, out) != 1) {
        free(pixel_data);
        return WRITE_ERROR;
    }

    free(pixel_data);

    return WRITE_OK;
}
