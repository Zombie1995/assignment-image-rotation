#include "bmp.h"
#include "image.h"
#include "image_rotation.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Wrong number of parameters\n");
    }
    else {
        printf("Processing\n");

        FILE *in = fopen(argv[1], "rb");
        if (in == NULL) {
            fprintf(stderr, "Can't open read file\n");
        }
        FILE *out = fopen(argv[2], "wb");
        if (out == NULL) {
            fprintf(stderr, "Can't open write file\n");
        }
        
        struct image *img = NULL;
        from_bmp(in, &img);
        struct image* rotated_img = rotate(img);
        to_bmp(out, rotated_img);

        free_image(img);
        free_image(rotated_img);
        fclose(in);
        fclose(out);

        printf("Done\n");
    }

    return 0;
}
