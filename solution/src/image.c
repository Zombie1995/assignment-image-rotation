#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image* create_image(uint32_t width, uint32_t height, struct pixel* data) {
    struct image* img = malloc(sizeof(struct image));

    img->width = width;
    img->height = height;
    img->data = data;

    // return (struct image){.width = width, .height = height, .data = data};
    return img;
}

void free_image(struct image* img) {
    free(img->data);
    free(img);
}
